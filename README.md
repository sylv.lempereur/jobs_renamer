# hcs_renamer

Python build:

[![Python:3_6](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/python3_6/build_3_6.svg?job=build3.6)](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/python3_6/build_3_6.log?job=build3.6)
[![Python:3_7](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/python3_7/build_3_7.svg?job=build3.7)](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/python3_7/build_3_7.log?job=build3.7)
[![Python:3_8](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/python3_8/build_3_8.svg?job=build3.8)](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/python3_8/build_3_8.log?job=build3.8)

[![coverage report](https://gitlab.com/sylv.lempereur/hcs_renamer/badges/master/coverage.svg)](https://gitlab.com/sylv.lempereur/hcs_renamer/commits/master)
[![pylint report](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/pylint/pylint.svg?job=pylint)](https://gitlab.com/sylv.lempereur/hcs_renamer/-/jobs/artifacts/master/raw/pylint/pylint.log?job=pylint)

Get informations from spreadsheets to rename Nikon Jobs acquisition.

Prerequisites
=============

Python
------

[openpyxl](https://openpyxl.readthedocs.io/en/stable)
have to be installed in a python 3.6 or 3.7 environment.

Setup
=====

To set this project up, use pip like this :

```bash
pip install .
```

This will install openpyxl, and creates two command line scripts:

* [sampleid_attribution](calls/sampleid_attribution) that creates a xlsx file with unique identifier
for every mounted sample
* [nikon_jobs_rename](calls/nikon_jobs_rename) will generated a correspondance between Jobs acquisition
and its corresponding plate, and will rsync this sample to the output folder
with a tps name.
# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
====================================================================
sampleid_attribution.nikon_jobs_rename.update_montage_after_renaming
====================================================================

Update mounting_plate_status to set last acquisition comments
and imaged status to imaged plate.
"""

from os import makedirs
from os.path import exists

from hcs_naming_sampleid_attribution._get_infos_from_mounting_status import\
    _get_infos_from_mounting_status

def update_montage_after_renaming(
        acquired: dict,
        path_mounting_status: str
        ):
    """
    Check if the destination folder exists, creates it otherwise.

    Parameters
    ----------
    path_output: str
        Path to the destination folder

    Returns
    -------
    boolean
        Exit status
    """

    # Get infos from get_mounting_plates
    elements = _loads_infos(path_montage)[1]

    return True

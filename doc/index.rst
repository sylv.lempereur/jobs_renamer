.. hcs_naming documentation master file, created by
   sphinx-quickstart on Thu Nov  7 11:41:26 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hcs_naming's documentation!
======================================

hcs_naming is a package to download informations and uses them to generate HCS tracking tools,
and rename Jobs acquired files.

It will run python scripts. These scripts are using:

- `openpyxl <https://openpyxl.readthedocs.io/en/stable>`_

If you want to install this project, clone this project,
go into your cloned directory and do::

    pip install .

Documentation
=============

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ./api/modules.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

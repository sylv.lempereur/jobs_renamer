hcs\_naming package
===================

Subpackages
-----------

.. toctree::

    hcs_naming.nikon_jobs_rename
    hcs_naming.sampleid_attribution
    hcs_naming.tools

Submodules
----------

hcs\_naming.version module
--------------------------

.. automodule:: hcs_naming.version
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: hcs_naming
    :members:
    :undoc-members:
    :show-inheritance:

hcs\_naming.nikon\_jobs\_rename package
=======================================

Submodules
----------

hcs\_naming.nikon\_jobs\_rename.create\_tps\_names module
---------------------------------------------------------

.. automodule:: hcs_naming.nikon_jobs_rename.create_tps_names
    :members:
    :undoc-members:
    :show-inheritance:

hcs\_naming.nikon\_jobs\_rename.rename\_nikon\_jobs\_acquisitions module
------------------------------------------------------------------------

.. automodule:: hcs_naming.nikon_jobs_rename.rename_nikon_jobs_acquisitions
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: hcs_naming.nikon_jobs_rename
    :members:
    :undoc-members:
    :show-inheritance:

hcs\_naming.sampleid\_attribution package
=========================================

Submodules
----------

hcs\_naming.sampleid\_attribution.create\_sample\_id\_xlsx module
-----------------------------------------------------------------

.. automodule:: hcs_naming.sampleid_attribution.create_sample_id_xlsx
    :members:
    :undoc-members:
    :show-inheritance:

hcs\_naming.sampleid\_attribution.update\_mounting\_plates\_status module
-------------------------------------------------------------------------

.. automodule:: hcs_naming.sampleid_attribution.update_mounting_plates_status
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: hcs_naming.sampleid_attribution
    :members:
    :undoc-members:
    :show-inheritance:

# Author:
#    Sylvain Lempereur<sylv.lempereur@gmail.com
"""
===================
tools.set_formating
===================

Change formating of mounting_plate_status to emphase sample status.
"""

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import\
    Font,\
    PatternFill
from openpyxl.styles.borders import\
    Border,\
    Side
from openpyxl.worksheet.datavalidation import DataValidation

from hcs_naming.tools import get_sample_status_sheets

def set_formating(path_dish_ids: str):
    """
    Set every useful formating on the given sheet.

    Parameters
    ----------
    path_dish_ids: str
        Path to the xlsx file to format

    Returns
    -------
    boolean
        Exit status
    """
    # Load the workbook
    book = load_workbook(filename=path_dish_ids)
    # Get sheets
    sheets = get_sample_status_sheets(book)
    status_dict = {
        -4: 'To solve',
        -3: 'Never analysed',
        -2: 'Never imaged',
        -1: 'None',
        0: 'Unknow',
        1: 'Imaged',
        2: 'Analysed',
        3: 'Unmounted samples stored',
        4: 'Unmounted samples destroyed'
        }

    # Creation of color dictionnary
    colors = {
        "black": "000000",
        "blue": "6666ff",
        "green_light": "66ff66",
        "grey_light": "CCCCCC",
        "magenta_light": "ff66ff",
        "magenta_dark": "963296",
        "orange": "BE9600",
        "red_light": "ff6666",
        "white": "ffffff",
        "yellow_light": "ffff66",
        }
    # Creation of styles dictionary
    formats = {
        'Analysed': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["yellow_light"],
                end_color=colors["yellow_light"]
                )
            },
        'border': Border(
            left=Side(style='thin'),
            right=Side(style='thin'),
            top=Side(style='thin'),
            bottom=Side(style='thin')
            ),
        'Imaged': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["magenta_light"],
                end_color=colors["magenta_light"]
                )
            },
        'Never imaged': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["magenta_dark"],
                end_color=colors["magenta_dark"]
                )
            },
        'Never analysed': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["orange"],
                end_color=colors["orange"]
                )
            },
        'None': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["black"],
                end_color=colors["black"]
                )
            },
        'To solve': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["blue"],
                end_color=colors["blue"]
                )
            },
        'Unknow': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["grey_light"],
                end_color=colors["grey_light"]
                )
            },
        'Unmounted samples destroyed': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["red_light"],
                end_color=colors["red_light"]
                )
            },
        'Unmounted samples stored': {
            "font": Font(color=colors["black"]),
            "background": PatternFill(
                "solid",
                start_color=colors["green_light"],
                end_color=colors["green_light"]
                )
            }
        }
    for sheet in sheets:
        sheet.column_dimensions["A"].width = len(
            sheets[0]["A1"].value
            )
        sheet.column_dimensions["B"].width = len(
            max(status_dict.values(), key=len)
            )
    # Add data validation
    formula = '"'
    for key, status in status_dict.items():
        if key >= 0:
            formula += status +', '
    formula += '"'
    data_validation = DataValidation(
        type="list",
        formula1=formula,
        allow_blank=False
        )
    # Apply colors
    row = 2
    while sheets[0][get_column_letter(1)+str(row)].value:
        data_validation.add(sheets[0]["B" + str(row)])
        status = sheets[0][
            get_column_letter(2) + str(row)
            ].value
        for sheet in sheets:
            sheet["A" + str(row)].fill = formats[status]["background"]
            sheet["B" + str(row)].fill = formats[status]["background"]
            sheet["A" + str(row)].border = formats["border"]
            sheet["B" + str(row)].border = formats["border"]
        col = 3
        while sheets[0][get_column_letter(col)+str(row)].value:
            cell_pos = get_column_letter(col)+str(row)
            sample_status = status
            # get the sample status
            sample_status = status_dict[
                sheets[4][cell_pos].value
                ]
            # Apply corresponding color to the cell in every sheet
            for sheet in sheets:
                sheet[cell_pos].border = formats['border']
                sheet[cell_pos].fill = formats[sample_status]["background"]
            col += 1
        row += 1
    sheets[0].add_data_validation(data_validation)
    # Hide second and forth sheets
    sheets[1].sheet_state = 'hidden'
    sheets[4].sheet_state = 'hidden'
    # Save results
    book.save(path_dish_ids)
    return True

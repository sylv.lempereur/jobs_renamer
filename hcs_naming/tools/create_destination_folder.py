# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
====================================================
sampleid_attribution.tools.create_destination_folder
====================================================

Check for each palte if plate status and sample status are compatible.
"""

from os import makedirs
from os.path import exists

def create_destination_folder(
        path_output: str
        ):
    """
    Check if the destination folder exists, creates it otherwise.

    Parameters
    ----------
    path_output: str
        Path to the destination folder

    Returns
    -------
    boolean
        Exit status
    """

    if not exists(path_output):
        makedirs(path_output)
    return True

# Author:
#    Sylvain Lempereur<sylv.lempereur@gmail.com
"""
=====
tools
=====

Description
===========

Contains functions used in several python scripts.

Functions
=========

========================= ======================================================
check_status              Check sample and plate status to cordinate them.
create_destination_folder Tests if destination exists and creates it otherwise.
get_headers               Get headers of a spreadsheet.
get_sample_status_sheets  Get sheets in mounting_plate_status.
set_formatting            Set convenient style formating to each cell.
========================= ======================================================
"""

from .check_status import check_status
from .create_destination_folder import create_destination_folder
from .get_headers import get_headers
from .get_sample_status_sheets import get_sample_status_sheets
from .set_formating import set_formating

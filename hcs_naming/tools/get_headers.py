# Author:
#    Sylvain Lempereur<sylv.lempereur@gmail.com
"""
=================
tools.get_headers
=================
Get every headers from a spreadsheet

Expect the first row to contains headers.
"""
from openpyxl import worksheet
from openpyxl.utils import get_column_letter

def get_headers(sheet: worksheet):
    """
    Get headers from a spreadsheet

    Parameters
    ----------
    sheet: openpyxl.worksheet
        Sheet that contains wanted headers

    Returns
    -------
    list
        list of headers
    """
    headers = []
    col = 1
    while sheet[get_column_letter(col)+ str(1)].value:
        headers.append(sheet[get_column_letter(col)+ str(1)].value)
        col += 1
    return headers

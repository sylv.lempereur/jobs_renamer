# Author:
#    Sylvain Lempereur<sylv.lempereur@gmail.com
"""
==============================
tools.get_sample_status_sheets
==============================

Get every sheets in mounting_plate_status
"""

from openpyxl import Workbook

def get_sample_status_sheets(book_sample_status: Workbook):
    """
    Get every sheet in mounting_plate_status

    Parameters
    ----------

    book_sample_status: openpyxl.Workbook
        mounting_plate_status loadedd as a openpyxl Workbook

    Returns
    -------
    list
        Every sheets in mounting_plate_status in this order:
        [
            'sample ids',
            TPS names bases',
            Acquisition comments',
            'Analysis comments',
            'sample status
        ]
    """
    sheets = []
    sheets.append(book_sample_status['sample ids'])
    sheets.append(book_sample_status['TPS names bases'])
    sheets.append(book_sample_status['Acquisition comments'])
    sheets.append(book_sample_status['Analysis comments'])
    sheets.append(book_sample_status['sample status'])
    return sheets

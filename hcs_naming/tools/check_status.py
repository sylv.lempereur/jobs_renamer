# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
# and sort functions by alphabetic order.
"""
=================================================
sampleid_attribution.check_status
=================================================

Check for each palte if plate status and sample status are compatible.
"""

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

from hcs_naming.sampleid_attribution._check_status_vs_sample_status import\
    _check_status_vs_sample_status

def check_status(
        path_dish_ids: str,
        saving: bool = True
        ):
    """
    Check if plate status and corresponding sample status are corrects.

    Parameters
    ----------
    path_dish_ids: str
        Path to mounting_plate_status file

    saving: bool
        Allows to save results or not.

    Returns
    -------
    boolean
        Exit status
    """
    # Load state of mounting_plate_status
    book_sample_status = load_workbook(filename=path_dish_ids)
    sheet_dish_ids = book_sample_status["sample ids"]
    sheet_sample_status = book_sample_status["sample status"]
    # for each plate in mounting_plate_status
    row = 2
    while sheet_dish_ids["A" + str(row)].value:
        # Get plate status
        status_plate = sheet_dish_ids["B" + str(row)].value
        # Get sample status
        status_sample = []
        column = 3
        while sheet_dish_ids[get_column_letter(column) + str(row)].value:
            status_sample.append(
                sheet_sample_status[get_column_letter(column) + str(row)].value
                )
            column += 1
        status_plate_checked,\
            status_sample_checked = _check_status_vs_sample_status(
                status_plate,
                status_sample
                )
        if status_plate_checked != status_plate:
            sheet_dish_ids["B" +str(row)].value = status_plate_checked
        elif status_sample != status_sample_checked:
            for index, status in enumerate(status_sample_checked):
                sheet_sample_status[get_column_letter(index + 3) + str(row)] =\
                    status
        row += 1
    if saving:
        book_sample_status.save(path_dish_ids)
    return True

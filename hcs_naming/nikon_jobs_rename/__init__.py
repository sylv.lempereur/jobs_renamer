"""
=================
nikon_jobs_rename
=================

Purpose
=======
Functions usde to read a correspondance HCS-Nikon boite.xlsx
and rename each corresponding HCS acquisition with its correpsonding TCF name

Functions
=========

============================== =================================================
create_tps_names               Generate a xlsx file containing TPS names
rename_nikon_jobs_acquisitions Rename jobs acquisitions
update_montage_xlsx            Update montage_xlsx to update imaged samplesIDs
============================== =================================================
"""

from ._get_acquisition_folders import _get_acquisition_folders
from ._get_jobs_infos import _get_jobs_infos
from ._get_plates import _get_plates
from ._get_tps_names import _get_tps_names

from .create_tps_names import create_tps_names
from .rename_nikon_jobs_acquisitions import rename_nikon_jobs_acquisitions

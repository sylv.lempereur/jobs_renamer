"""
================================================
nikon_jobs_rename.rename_nikon_jobs_acquisitions
================================================

Use naming xlsx file to rename a nikon Jobs acquisition
and set it in a project specific folder.
"""

from os import\
    listdir,\
    rename
from os.path import exists, split

import subprocess

from hcs_naming.tools import create_destination_folder

from ._get_acquisition_folders import _get_acquisition_folders
from ._get_jobs_infos import _get_jobs_infos
from ._get_tps_names import _get_tps_names

def rename_nikon_jobs_acquisitions(
        folder_input: str,
        naming_xlsx: str,
        connection_xlsx: str,
        folder_output: str
        ):
    """
    Use the naming_xlsx file to rename Jobs acquired datasets.

    Parameters
    ----------
    folder_input: str
        Folder were acquired datasets are stored

    naming_xlsx: str
        Path to the xlsx file that contains TPS names

    connection_xlsx:str
        Path to the file that contains connection
        Between jops acquisition and TPS dish IDs

    folder_output: str
        Path were rename files will be

    Returns
    -------
    dict
        Dictionary of acquired samples in acquisition plates
    """
    if folder_output[-1] != "/":
        folder_output += "/"
    # To keep track of form-proposed comments, get connection infos.
    connection_infos = _get_jobs_infos(connection_xlsx)
    # If the output folder does not exist
    create_destination_folder(folder_output)
    if exists(folder_output + "debug.log"):
        rename(folder_output + "debug.log", folder_output + "debug.log.bak")
    log_file = open(folder_output + "debug.log", "x+")
    # Load naming_xlsx for every acquisitions
    acquisitions = _get_tps_names(naming_xlsx)
    # Creation of an acquired dictionary
    acquired = {}
    # For every acquistion folder in the input folder
    for folder in sorted(_get_acquisition_folders(folder_input)):
#        print(folder)
        folder_base = split(folder)[-1]
#        print(folder_base)
        # If this folder is into the correspondance file
        if folder_base in acquisitions:
            files_to_move = []
            acquired_samples = []
            # for every file in this folder
            debug = True
            for well in sorted(listdir(folder)):
                # check if "Well" is in the name
                if "Well" in well:
                    # Get well position
                    well_pos = int(
                        well[well.index("Well") + 4:well.index("_")]
                        ) - 1
                    if well_pos >= len(acquisitions[folder_base]["samples"]):
                        debug = False
                        log_file.write(
                            "="*80\
                            + "\n")
                        log_file.write(
                            folder\
                            + " well n°"\
                            + str(well_pos + 1)\
                            + " \n"\
                            + "-" * (len(folder) + 8 +len(str(well_pos)))\
                            + "\n"
                            )
                        log_file.write(
                            "Unexpected well value : too high"\
                            + "\n")
                    elif acquisitions[
                            folder_base
                        ]["samples"][well_pos] == "None":
                        debug = False
                        log_file.write(
                            "="*80\
                            + "\n")
                        log_file.write(
                            folder\
                            + " well n°"\
                            + str(well_pos + 1)\
                            + " \n"\
                            + "-" * (len(folder) + 8 +len(str(well_pos)))\
                            + "\n"
                            )
                        log_file.write(
                            "Unexcpeted well value : None"\
                             + "\n")
                    else:
                        tps_name = acquisitions[
                            folder_base
                            ][
                                "samples"
                                ][
                                    well_pos
                                    ]
                        folder_dest =\
                            folder_output\
                            + acquisitions[
                                folder_base
                                ][
                                    "samples"
                                    ][
                                        well_pos
                                        ].split("_")[3]\
                            + "/"
                        create_destination_folder(folder_dest)

                        files_to_move.append(
                            [
                                folder\
                                    + "/"\
                                    + well,
                                folder_dest\
                                    + tps_name
                                ]
                            )

                        acquired_samples.append(
                            tps_name[
                                tps_name.index("_") + 1:\
                                    1 \
                                    + tps_name.index("_")\
                                    + tps_name[
                                        tps_name.index("_")+ + 1:
                                        ].index("_")
                                ]
                            )
#            print(debug)
            if debug:
                for moves in files_to_move:
#                    create_destination_folder(folder_dest)
                    subprocess.call(
                        [
                            #"echo", # Set for test reason. Should be remove
                            "mv",
                            moves[0],
                            moves[1]
                        ]
                        )
                acquired[acquisitions[folder_base]["plate"]] = {
                    "samples": acquired_samples,
                    "comment": connection_infos[folder_base]["comment"]
                    }
    # print(acquired)
    return acquired

"""
=================
nikon_jobs_rename._get_jobs_infos
=================

Get infos that make a correspondance between an acquisition and
its corresponding plate.
"""

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

from hcs_naming.tools import get_headers

def _get_jobs_infos(connection_xlsx: str):
    """
    Get information of Jobs acquisitions.

    Return a dictionary with every imaged plates.

    Parameters
    ----------
    connection_xlsx: str
        Path to the connection file

    Returns
    -------
    dict
        A dictionary containing every connection informations

    """
    # Open the xlsx file
    book = load_workbook(connection_xlsx)
    # Get the sheet that contains infos
    sheet = book["correspondance"]
    # Creation of the list of headers
    headers = get_headers(sheet)
    infos = {}
    row = 2
    # go trough each row
    while sheet[
            get_column_letter(
                headers.index("Folder Name") + 1
                ) + str(row)].value:
        # Get dossier
        dossier = sheet[
            get_column_letter(
                headers.index("Folder Name") + 1
                ) + str(row)
            ].value
        # Get connected box
        boite = sheet[
            get_column_letter(
                headers.index("Plate") + 1
                ) + str(row)
            ].value
        # Get connected status
        etat = sheet[
            get_column_letter(
                headers.index("Status") + 1
                ) + str(row)
            ].value
        # Get acquisition order
        ordre = sheet[
            get_column_letter(
                headers.index("Order") + 1
                ) + str(row)
            ].value
        # Get acquirer identifier
        acquisiteur = sheet[
            get_column_letter(
                headers.index("Acquirer") + 1
                ) + str(row)
            ].value
        # Get resolution
        resolution = sheet[
            get_column_letter(
                headers.index("Resolution") + 1
                ) + str(row)
            ].value
        comment = sheet[
            get_column_letter(
                headers.index("Comment") + 1
                ) + str(row)
            ].value
        if isinstance(resolution, float):
            resolution = int(resolution)
        resolution = str(resolution)
        # Add these infos into the dictionary
        infos[dossier] = {
            "plate": boite,
            "status": etat,
            "order": ordre,
            "acquirer": acquisiteur,
            "resolution": resolution,
            "comment": comment,
            }
        row += 1
    # Return the dictionary
    return infos

"""
================================
nikon_jobs_rename._get_tps_names
================================

Get tps names for every acquisition using a xls file.
"""
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

def _get_tps_names(xlsx_naming: str):
    """
    Get tps names for every acquisitions.
    using the xlsx naming xlsx file.

    Parameters
    ----------
    xlsx_naming: str
        Path to xlsx file that contains TPS names

    Returns
    -------
    Dict
        Dictionary containing every acquired plates
        and tps names for each sample.
    """
    # Open naming_xlsx
    book = load_workbook(xlsx_naming)
    # Get tps naming sheet
    sheet = book["hcs tps naming"]
    row = 2
    # Creation of the reutrned dictionary
    acquisitions = {}
    # Since row contains a plate
    while sheet["A" + str(row)].value:
        samples = []
        col = 3
        # Get every tps names in this acquisitions
        while sheet[get_column_letter(col)+ str(row)].value:
            samples.append(sheet[get_column_letter(col)+ str(row)].value)
            col += 1
        acquisitions[sheet["B" + str(row)].value] = {
            "samples": samples,
            "plate": sheet["A" + str(row)].value
            }
        row += 1
    # Return the dictionary
    return acquisitions

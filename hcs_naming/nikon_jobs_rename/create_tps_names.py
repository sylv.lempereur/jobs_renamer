"""
==================================
nikon_jobs_rename.create_tps_names
==================================

Get tps names for every acquisition.
"""

from openpyxl import Workbook
from openpyxl.utils import get_column_letter

from ._get_jobs_infos import _get_jobs_infos
from ._get_plates import _get_plates

def create_tps_names(
        connection_xlsx: str,
        montage_xlsx: str,
        output_xlsx: str,
        saving: bool = True
        ):
    """
    Generate a xlsx file that create TPS name for every jobs acquisition.
    To do so, infos from the connection xlsx is required.

    Parameters
    ----------
    connection_xlsx: str
        Path to the connection xlsx file

    montage_xlsx: str
        Path to the boite_des_montages_with_specimenIDS.xlsx
        This file contains every informations used to create TPS names

    output_xlsx: str
        Path used to saved the generated xlsx file.

    saving: bool
        (Optionnal) allows to save result of the computation

    Returns
    -------
    Boolean
        Exit status
    """
    print(
        " Create a xlsx file that contains TPS name for HCS acquisitions."
        )
    infos = _get_jobs_infos(connection_xlsx)
    book_output = Workbook()
    book_output.active.title = 'hcs tps naming'

    headers = [
        "plateId",
        "acquisition folder"
        ]

    for i in range(1, 97):
        headers.append(
            "Nikon well n° " + str(i)
            )

    for col, header in enumerate(headers):
        book_output['hcs tps naming'][
            get_column_letter(col + 1) + str(1)
            ].value = header

    plates_naming = _get_plates(montage_xlsx)

    row = 2
    for acquisition in infos:
        if infos[acquisition]["plate"] in plates_naming:
            book_output['hcs tps naming']["A" + str(row)]\
                = infos[acquisition]["plate"]
            book_output['hcs tps naming']["B" + str(row)] = acquisition
            for index, sample in enumerate(
                    plates_naming[
                        infos[acquisition]["plate"]
                    ][
                        ::int(
                            infos[acquisition]["order"]
                        )
                    ]
                ):
                if sample != "None":
                    book_output['hcs tps naming'][
                        get_column_letter(3 + index) + str(row)
                        ].value =\
                        acquisition[2:8]\
                        + infos[acquisition]["acquirer"]\
                        + '_'\
                        + sample\
                        + '_'\
                        + infos[acquisition]["resolution"]\
                        + "_Nh_"\
                        + infos[acquisition]["status"]\
                        + '.nd2'
                else:
                    book_output['hcs tps naming'][
                        get_column_letter(3 + index) + str(row)
                        ].value = 'None'
            row += 1
    if saving:
        book_output.save(output_xlsx)
    return True

"""
=================
nikon_jobs_rename._get_plates
=================

Get every plate in get_mounting_plate and create a dictionnary with them.

"""

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

def _get_plates(
        montage_xlsx: str
        ):
    """
    Get every plates in get_mounting_plate.xlsx
    and create a dictionnary with them.

    Paremeters
    ----------
    montage_xlsx: str
        Path to the boite_de_montages_with_sampleIds.xlsx

    Returns
    -------
    dict
        Dictionary containing each plate with its samples
    """
    book = load_workbook(montage_xlsx, data_only=True)
    sheet = book["TPS names bases"]
    plates = {}
    row = 2
    while sheet["A" + str(row)].value:
        plate = sheet["A" + str(row)].value
        samples = []
        col = 3
        while sheet[get_column_letter(col) + str(row)].value:
            samples.append(sheet[get_column_letter(col) + str(row)].value)
            col += 1
        plates[plate] = samples
        row += 1
    return plates

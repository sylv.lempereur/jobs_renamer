"""
================================================
nikon_jobs_rename._get_acquisition_folders
================================================

Get every Jobs acquisition folder paths in a given folder.
"""

from os import listdir
from os.path import isdir


from re import match as match_regex

def _get_acquisition_folders(folder_look_up):
    """
    Get any folders with an name compatible with a nikon Jobs acquisition
    into a looked up folder.

    Parameters
    ----------

    folder_look_up:str
        Path for input folder

    Returns
    -------

    list
        list of  acquisition folder's paths
    """
    if folder_look_up[-1] != '/':
        folder_look_up += '/'
    paths_acquisitions = []
    for file in sorted(listdir(folder_look_up)):
        if isdir(folder_look_up + file):
            paths_acquisitions_temp = _get_acquisition_folders(
                folder_look_up + file
                )
            for path in paths_acquisitions_temp:
                paths_acquisitions.append(path)
            del paths_acquisitions_temp
            if match_regex(
                    r"\d{8}_\d{6}_\d{3}",
                    file
                ):
#                print(folder_look_up + file)
                paths_acquisitions.append(folder_look_up + file)
    return paths_acquisitions

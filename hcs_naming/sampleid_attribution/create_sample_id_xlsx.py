# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
# and sort functions by alphabetic order.
"""
==========================================
sampleid_attribution.create_sample_id_xlsx
==========================================

Creation of mounting_plate_status using archive and get_mounting_plate.
Calling sampleid_attribution,
this function will be used if no mounting_plate_status is provided.

"""

from openpyxl import Workbook

from ._create_row import _create_row
from ._get_general_infos import _get_general_infos
from ._loads_infos import _loads_infos

def create_sample_id_xlsx(
        path_dish: str,
        path_dish_ids: str,
        path_archive: str
        ):
    """
    Create a xlsx file containing a unique idnetifier
    for every sample mounted in a plate.

    This function uses suivi des boite de montages and archive spreadsheets
    to creates unique identifiers for each mounted samples.

    Parameters
    ----------

    path_dish: str
        Path to the local copy of suivi des boites de montages
    path_archive: str
        Path to the local copy of archive
    path_dish_ids: str
        Path used to saved results of this computation

    Returns
    -------

    boolean
        Expression of the success of the computation
    """
    print("Create a xlsx containing unique specimen IDs.")
    #Get General infos(exp id, corresponding projects, corresponding sample age)
    generals_infos = _get_general_infos(path_archive)
    #Get plates identifgiers and corrsponding samples
    headers, elements = _loads_infos(path_dish)
    #Creation of the output file
    book_output = Workbook()
    # Creation of sheets sample ids and TPS name bases and put them on a list
    sheets = [book_output.active]
    sheets[0].title = "sample ids"
    sheets.append(book_output.create_sheet("TPS names bases"))
    sheets.append(book_output.create_sheet("Acquisition comments"))
    sheets.append(book_output.create_sheet("Analysis comments"))
    sheets.append(book_output.create_sheet("sample status"))
    #Creation of a dictionnary used to new wich letter to attribute to a sample
    samples_reference = {}
    # Write previously known headers on each sheet
    for sheet in sheets:
        sheet.cell(
            row=1,
            column=1,
            value=headers[0]
            )
        sheet.cell(
            row=1,
            column=2,
            value="Status"
            )
        for col, value in enumerate(headers[1:]):
            sheet.cell(
                row=1,
                column=3 + col,
                value=value
                )
    #For each row in suivi_des_boites_de_montage
    for row, values in enumerate(elements):
        book_output, samples_reference = _create_row(
            book_dish_ids=book_output,
            row=row + 2,
            element=values,
            infos_archive=generals_infos,
            samples_reference=samples_reference
            )

    # Save the sheet into the correspondinc file.
    book_output.save(path_dish_ids)
    return True

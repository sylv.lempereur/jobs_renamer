# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
================================
sampleid_attribution._update_cell
================================

Change value for a given cell in mounting_plate_status

See also
--------
_create_row
"""

from openpyxl import Workbook
from openpyxl.utils import get_column_letter

def _update_cell(
        book_dish_ids: Workbook,
        positions: tuple,
        exp_id: int,
        infos_archive: dict,
        samples_reference: dict
        ):
    """
    Update given cell in every sheet of mounting_plate_status

    Parameters
    ----------

    book_dish_ids: openpyxl.Workbook
        Workbook to modify

    positions: tupple
        (row,column) positions of the cell

    exp_id: int
        experiment id

    infos_archive: dict
        Dictionary of infos from archive.xlsx

    samples_reference: dict
        current number of samples for each experiment id

    Returns
    -------

    openpyxl.Workbook
        Updated workbook

    Dict
        Modified samples_reference

    See also
    --------
    _create_row
    """
    if isinstance(exp_id, (int, float)):
        exp_id = str(int(exp_id))
    if exp_id in samples_reference:
        samples_reference[exp_id] += 1
    elif exp_id == "None":
        book_dish_ids\
            ["sample ids"]\
            [get_column_letter(positions[1]) + str(positions[0])] = "None"
        book_dish_ids\
            ["TPS names bases"]\
            [get_column_letter(positions[1]) + str(positions[0])] = "None"
        book_dish_ids\
            ['sample status']\
            [get_column_letter(positions[1]) + str(positions[0])] = -1
        return book_dish_ids, samples_reference
    else:
        samples_reference[exp_id] = 1
    sample_id = str(exp_id)\
        + get_column_letter(samples_reference[exp_id]).lower()
    specimen_id = sample_id \
        + '_' \
        + infos_archive[exp_id][1] \
        + '_' \
        + infos_archive[exp_id][0]
    book_dish_ids\
        ["sample ids"]\
        [get_column_letter(positions[1]) + str(positions[0])] = sample_id
    book_dish_ids\
        ["TPS names bases"]\
        [get_column_letter(positions[1]) + str(positions[0])] = specimen_id
    book_dish_ids\
        ['sample status']\
        [get_column_letter(positions[1]) + str(positions[0])] = 0
    return book_dish_ids, samples_reference

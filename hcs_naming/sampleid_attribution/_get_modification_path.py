"""
======================================================
hcs_naming.sampleid_attribution._get_modification_path
======================================================
Return the path to the file used to store modification
in mounting_plate_status.

This path leads to the rfolder containing mounting_plate_status.
"""

from os.path import split

def _get_modification_path(path: str):
    """
    Return the path to the file used to store modification
    in mounting_plate_status.

    This path leads to the rfolder containing mounting_plate_status.

    Parameters
    ----------

    path: str
        Path to mounting_plate_status

    Returns
    -------

    str
        Path to "modifications.log"
    """
    if not split(path)[0]:
        path_to_modification = 'modifications.log'
    else:
        path_to_modification = split(path)[0] + "/modifications.log"
    return path_to_modification

# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
====================================================
sampleid_attribution._check_status_vs_samples_status
====================================================

Check status plates and their corredponding samples to set them accordingly.

See Also
--------
_check_analyzed
_check_imaged
_check_unknow
"""

from hcs_naming.sampleid_attribution._check_analyzed import _check_analyzed
from hcs_naming.sampleid_attribution._check_imaged import _check_imaged
from hcs_naming.sampleid_attribution._check_unknow import _check_unknow

def _check_status_vs_sample_status(
        status: str,
        sample_status: list):
    """
    Check if the status of a sample is compatible with the plate status,
    and if the status of the plate have to be change

    Parameters
    ----------
    status: str
        General status of the plate
    sample_status: list
        Status of each sample in this plate

    Returns
    -------
    str
        New status
    list
        List of new sample status
    """
    if status == 'Analysed':
        status, sample_status = _check_analyzed(sample_status)
    elif status == 'Imaged':
        status, sample_status = _check_imaged(sample_status)
    else:
        status, sample_status = _check_unknow(sample_status)
    return status, sample_status

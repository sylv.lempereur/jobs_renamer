# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
==================================
sampleid_attribution._check_imaged
==================================

Check if sample status in a mounting plate corresponf to the "Imaged" status

See Also
--------
_check_analyzed
_check_status_vs_sample
_check_unknow
"""

def _check_imaged(sample_status):
    """
    Assumed a plate status of "Imaged" and check
    if each sample is compatible with this status.

    Parameters
    ----------
    sample_status: list
        List of sample status to check

    Returns
    ------
    str
        Plate status
    list
        List of checked sample status
    """
    status = "Imaged"
    # If at least one sample status is unkonwn
    if any(sample == 0 for sample in sample_status):
        # if every reliable sample status is unknown
        if all(sample < 1 for sample in sample_status):
            # Set every status to analyzed
            sample_status = [
                1 if sample == 0\
                else sample\
                for sample in sample_status
                ]
        # Else set them to 'we don't want to image them'.
        else:
            sample_status = [
                -2 if sample == 0\
                else sample\
                for sample in sample_status
                ]
    # If at least one sample status is analyzed
    if any(sample == 2 for sample in sample_status):
        # If no sample status is imaged
        if all(sample != 1 for sample in sample_status):
            # Set them all to analyzed
            status = "Analysed"
    # Return everything usefull
    return status, sample_status

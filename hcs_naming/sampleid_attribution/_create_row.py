# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
================================
sampleid_attribution._create_row
================================

Add a row to mounting_plate_status

See also
--------
_update_cell
"""

from openpyxl import Workbook

from ._update_cell import _update_cell

def _create_row(
        book_dish_ids: Workbook,
        row: int,
        element: list,
        infos_archive: dict,
        samples_reference: dict
        ):
    """
    Add a new row of informations into the given workbook.

    Parameters
    ----------
    book_dish_ids: openpyxl.Workbook
        Workbook to change

    row: int
        position to add data

    element: list
        list of elements contaning plateId in first position
        and experiment Ids in other positions

    infos_archive: dict
        Dictionary of infos from archive.xlsx

    samples_reference: dict
        current number of samples for each experiment id

    Returns
    -------

    openpyxl.Workbook
        Updates workbook

    Dict
        Modified samples_reference

    See also
    --------
    _update_cell
    """
    print('\t add row')
    sheet_names = [
        "TPS names bases",
        "sample status",
        "Acquisition comments",
        "Analysis comments"]
    book_dish_ids["sample ids"]["A"+str(row)] = element[0]
    book_dish_ids["sample ids"]["B"+str(row)] = "Unknow"
    for sheet_name in sheet_names:
        book_dish_ids[sheet_name]["A"+str(row)] = element[0]
        book_dish_ids[sheet_name]["B"+str(row)] = "='sample ids'!B"\
            + str(row)
    for index, exp_id in enumerate(element[1:]):
        book_dish_ids, samples_reference = _update_cell(
            book_dish_ids,
            (row, index+3),
            exp_id,
            infos_archive,
            samples_reference
            )
    return book_dish_ids, samples_reference

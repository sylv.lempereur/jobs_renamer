# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
==================================
sampleid_attribution._check_unknow
==================================

Check if sample status in a mounting plate corresponf to the "Imaged" status

See Also
--------
_check_analyzed
_check_status_vs_sample
_check_unknow
"""

def _check_unknow(sample_status):
    """
    Assumed a plate status of "Unknow" and check
    if each sample is compatible with this status.

    Parameters
    ----------
    sample_status: list
        List of sample status to check

    Returns
    ------
    str
        Plate status
    list
        List of checked sample status
    """
    status = "Unknow"
    # If no element is set to "Unknow"
    if all(sample != 0 for sample in sample_status):
        # If at least one sample status is imaged
        if any(sample == 1 for sample in sample_status):
            # Set general status to Imaged
            status = "Imaged"
        else:
            # else, set it to "Analysed"
            status = "Analysed"
    return status, sample_status

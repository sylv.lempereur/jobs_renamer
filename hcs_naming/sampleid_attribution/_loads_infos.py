# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
=======================================
sampleid_attribution._loads_infos
=======================================

Loads infos from get_mounting_plate

See also
--------
_get_general_infos
_get_ref_infos
"""

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

from hcs_naming.tools import get_headers

def _loads_infos(
        xlsx_path: str
        ):
    """
    Get usefull informations from the "mounting_plate_status".

    Return plate unique Id and experiment ids
    for each monted sample into this plate.

    Parameters
    ----------

    xlsx_path: str
        Path to the local copy of the mounting_plate_status

    Returns
    -------
    list
        List of headers

    list
        Return a list of list with plateId and corresponding samples.

    """
    # load workbook
    book = load_workbook(xlsx_path)
    sheet = book["plates"]
    elements = []
    # Get headers
    headers = get_headers(sheet)#
    # Find where usefull piece of data are stored
    indexes = [headers.index('dish identifier')]
    for header in headers:
        if 'sample' in header:
            indexes.append(headers.index(header))
    # Get usefull elements (plateId and corrsponding samples)
    row = 2
    while sheet["A" + str(row)].value:
        elements.append([])
        for col in indexes:
            value = sheet[
                get_column_letter(col + 1) + str(row)
                ].value
            if value:
                # Potential float will be floored.
                if isinstance(value,
                              float
                              ):
                    value = int(value)
                elements[-1].append(value)
        row += 1
    headers_usefull = []
    for index in indexes:
        headers_usefull.append(headers[index])
    return headers_usefull, elements

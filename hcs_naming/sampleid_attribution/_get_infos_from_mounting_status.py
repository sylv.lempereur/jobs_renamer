# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
====================================================
sampleid_attribution._get_infos_from_mounting_status
====================================================

Get infos from the current version of mounting_plate_status.

See also
--------
_update_cell
"""

from openpyxl import worksheet
from openpyxl.utils import get_column_letter
from openpyxl.utils.cell import column_index_from_string

def _get_infos_from_mounting_status(
        sheet: worksheet
        ):
    """
    Extract informations from the current status of mounting_plate_status

    Parameters
    ----------

    sheet: openpyxl.worksheet
        Sheet containing sampleId on mounting_plate_status

    Returns
    -------
    list
        [samples, plates, row]

        samples is a dictionary which contains the last identifier
        for a experiment Id.

        plates is a dictionary containing samples and row
        for each plate in mounting_plate_status

        row is the first empty row
    """
    plates = {}
    samples = {}
    row = 2
    while sheet["A" + str(row)].value:
        col = 3
        plate = sheet["A" + str(row)].value
        sample_in_plates = []
        while sheet[get_column_letter(col) + str(row)].value:
            value = sheet[get_column_letter(col) + str(row)].value
            if value != "None":
                index = 0
                while value[index : index + 1].isdigit():
                    index += 1
                sample_id = [
                    value[:index],
                    column_index_from_string(value[index:])
                    ]
                sample_in_plates.append(int(sample_id[0]))
                if sample_id[0] in samples:
                    if sample_id[1] > samples[sample_id[0]]:
                        samples[sample_id[0]] = sample_id[1]
                else:
                    samples[sample_id[0]] = sample_id[1]
            else:
                sample_in_plates.append("None")
            plates[plate] = {
                'row': row,
                'samples': sample_in_plates
                }
            col += 1
        row += 1
    return [samples, plates, row]

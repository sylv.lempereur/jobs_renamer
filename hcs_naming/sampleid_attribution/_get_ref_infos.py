# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
===================================
sampleid_attribution._get_ref_infos
===================================

Loads infos from initial state of mounting_plate_status.

See also
--------
_get_general_infos
_loads_infos
"""

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

from ._check_status_vs_sample_status import _check_status_vs_sample_status

def _get_ref_infos(path_ref: str):
    """
    open the reference file to get the current status, sample status and
    acquisition comments of each box.

    Parameters
    ----------
    path_ref: str
        Path to the reference xlsx file

    Returns
    -------
    dict
        A dictionnary with the status of each plate, of each sample
        and acquisition comment for each sample
    """
    book = load_workbook(path_ref)
    sheet = book['sample ids']
    sheet_acquisitions_comments = book['Acquisition comments']
    sheet_analysis_comments = book['Analysis comments']
    sheet_status = book['sample status']
    ref_infos = {}
    row = 2
    while sheet[get_column_letter(1) + str(row)].value:
        plate = sheet[get_column_letter(1) + str(row)].value
        status = sheet[get_column_letter(2) + str(row)].value
        sample_status = []
        sample_comments = []
        analysis_comments = []
        col = 3
        while sheet[get_column_letter(col) + str(row)].value:
            if sheet[
                    get_column_letter(col) + str(row)
                ].value != 'None':
                temp_status = sheet_status[
                    get_column_letter(col) + str(row)
                    ].value
            else:
                temp_status = -1
            sample_status.append(temp_status)
            sample_comments.append(
                sheet_acquisitions_comments[
                    get_column_letter(col) + str(row)
                ].value
                )
            analysis_comments.append(
                sheet_analysis_comments[
                    get_column_letter(col) + str(row)
                ].value
                )
            col += 1
        # Check if status and sample_status are compatible
        status, sample_status = _check_status_vs_sample_status(
            status,
            sample_status
            )
        ref_infos[plate] = {
            "status": status,
            "sample_status": sample_status,
            "acquisition comments": sample_comments,
            "analysis comments": analysis_comments
            }
        row += 1
    return ref_infos

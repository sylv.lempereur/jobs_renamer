# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
====================================
sampleid_attribution._check_analyzed
====================================

check if sample status in a mounting plate corresponds to the "Analyzed" status.

See Also
--------
_check_imaged
_check_status_vs_sample
_check_unknow
"""

def _check_analyzed(sample_status):
    """
    Assumed a plate status of "Analysed and check
    if each sample is compatible with this status.

    Parameters
    ----------
    sample_status: list
        List of sample status to check

    Returns
    ------
    str
        Plate status
    list
        List of checked sample status
    """
    # If at least one sample status is unkonwn
    if any(sample == 0 for sample in sample_status):
        # if every reliable sample status is unknown
        if all(sample < 1 for sample in sample_status):
            # Set every status to analyzed
            sample_status = [
                2 if sample == 0\
                else sample\
                for sample in sample_status
                ]
        # Else set them to
        # 'we don't know if it wasn't imaged or analyzed'.
        else:
            sample_status = [
                -4 if sample == 0\
                else sample\
                for sample in sample_status
                ]
    # If at least one sample status is imaged
    if any(sample == 1 for sample in sample_status):
        # If no sample status is analyzed
        if all(sample != 2 for sample in sample_status):
            # Set them all to analyzed
            sample_status = [
                2 if sample == 1\
                else sample\
                for sample in sample_status
                ]
        else:
            # Else, set them to 'will not be analyzed'
            sample_status = [
                -3 if sample == 1\
                else sample\
                for sample in sample_status
                ]
    # Return everything useful
    return "Analysed", sample_status

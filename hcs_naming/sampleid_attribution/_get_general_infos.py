# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
# and sort functions by alphabetic order.
"""
=======================================
sampleid_attribution._get_general_infos
=======================================

Loads infos from archive

See also
--------
_get_ref_infos
_loads_infos
"""

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

def _get_general_infos(
        path_archive: str
        ):
    """
    Get every experiment numbers
    with the corresponding project Id and sample age.

    Extract this information from the archive spreadsheet.

    Parameters
    ----------

    path_archive: str
        Path to the local copy of the archive spreadsheet

    Returns
    -------
    dict
        A dictionnary putting together experiment numbers
        with corresponding sample age and project ID.
        Structure : Dict[exp_id]=[project_id, sample_age]
    """
    book = load_workbook(path_archive)
    sheet = book['fusion']
    experiment_infos = {}
    row = 2
    while sheet[get_column_letter(2) + str(row)].value:
        if isinstance(sheet[
                        get_column_letter(2) + str(row)
                        ].value,
                      (int, float)
                      ):
            exp_id = str(
                int(
                    sheet[get_column_letter(2) + str(row)].value
                    )
                )
            project_id = sheet[
                get_column_letter(5) + str(row)
                ].value
            sample_age = sheet[
                get_column_letter(10) + str(row)
                ].value
            if exp_id not in experiment_infos:
                if sample_age:
                    if " " in sample_age:
                        sample_age = sample_age.replace(" ", "")
                else:
                    sample_age = 'unknow'
                if project_id:
                    experiment_infos[exp_id] = [
                        project_id,
                        sample_age
                        ]
                else:
                    experiment_infos[exp_id] = [
                        "None",
                        sample_age
                        ]
        row += 1
    return experiment_infos

# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
# and sort functions by alphabetic order.
"""
=================================================
sampleid_attribution.update_mounting_plate_status
=================================================

Update mounting_plate_status using infos from get_mounting_plate.
"""

from datetime import datetime

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

from ._create_row import _create_row
from ._get_infos_from_mounting_status import _get_infos_from_mounting_status
from ._get_general_infos import _get_general_infos
from ._get_modification_path import _get_modification_path
from ._loads_infos import _loads_infos
from ._update_cell import _update_cell

def update_mounting_plates_status(
        path_dish_ids: str,
        path_montage: str,
        path_archive: str,
        saving: bool = True
        ):
    """
    Update of mounting_plate_status by getting modification
    from get_mounting plate

    Parameters
    ----------
    path_dish_ids: str
        Path to mounting_plate_status file

    path_montage: str
        Path to get_moutning_plate file

    path_archive: str
        Path to archive file

    saving: bool
        (optionnal) Allows to save or not the results of this functions

    Returns
    -------
    boolean
        Exit status
    """
    file_error = open(_get_modification_path(path_montage), "a")
    file_error_status = False
    # Load useful infos in archive
    infos_archive = _get_general_infos(path_archive)
    # Load previous state of mounting_plate_status
    book_dish_ids = load_workbook(filename=path_dish_ids)
    mounting_infos = _get_infos_from_mounting_status(
        book_dish_ids["sample ids"]
        )
    # Get infos from get_mounting_plates
    elements = _loads_infos(path_montage)[1]
    # Compare previous version of mounting_plate_status with get_mounting_plates
    for element in elements:
        # If the current plate already exists in mounting_plate_status
        if element[0] in mounting_infos[1]:
            # Get samples from get_mounting_plates
            samples = element[1:]
            # Get row of this plate in mounting_plate_status
            row = mounting_infos[1][element[0]]["row"]
            # both list of samples are differents
            if samples != mounting_infos[1][element[0]]["samples"]:
                if not file_error_status:
                    file_error.write("=" * 80 + '\n')
                    file_error.write(
                        datetime.now().strftime("%y/%m/%d, %H:%M:%S") + '\n'
                        )
                    file_error.write("=" * 80 + '\n')
                    file_error_status = True
                file_error.write(
                    element[0]\
                    + '\n'\
                    + "-" * len(element[0])\
                    + "\n"
                    )
                file_error.write(
                    "Position"\
                    + '\t'\
                    + "Former TPS name base"\
                    + "\t"
                    + "New TPS name base"\
                    + "\t"
                    + "Renamed"\
                    + "\n"
                    )
                # Chek if the difference is in the existing list
                for index, value in enumerate(
                        mounting_infos[1]\
                            [element[0]]\
                            ["samples"]):
                    if value != samples[index]:
                        file_error.write(
                            str(index + 1)\
                            + "\t"
                            )
                        file_error.write(
                            book_dish_ids[
                                "TPS names bases"
                                ][
                                    get_column_letter(index + 3)\
                                    + str(mounting_infos[1][element[0]]["row"])
                                    ].value
                            + "\t"
                            )
                        book_dish_ids, mounting_infos[0] = _update_cell(
                            book_dish_ids,
                            positions=(
                                row,
                                index\
                                    + 3),
                            exp_id=samples[index],
                            infos_archive=infos_archive,
                            samples_reference=mounting_infos[0]
                            )
                        file_error.write(
                            book_dish_ids[
                                "TPS names bases"
                                ][
                                    get_column_letter(index + 3)\
                                    + str(mounting_infos[1][element[0]]["row"])
                                    ].value
                            + "\t"
                            + "False"\
                            + "\n"
                            )
                # Add missing elements if well were forget
                for index, value in enumerate(
                        samples\
                        [len(
                            mounting_infos[1]\
                                [element[0]]\
                                ["samples"]
                        ): len(samples)]):
                    book_dish_ids, mounting_infos[0] = _update_cell(
                        book_dish_ids,
                        positions=(
                            row,
                            index\
                                + len(mounting_infos[1][element[0]]["samples"])\
                                + 3),
                        exp_id=value,
                        infos_archive=infos_archive,
                        samples_reference=mounting_infos[0]
                        )
                    file_error.write(
                        str(index + 1)\
                        + "\t"\
                        + "None"\
                        + "\t"\
                        + book_dish_ids[
                            "TPS names bases"
                            ][
                                get_column_letter(index + 3)\
                                + str(mounting_infos[1][element[0]]["row"])
                                ].value
                        + "\t"
                        + "False"
                        + "\n"
                        )
        else:
            book_dish_ids, mounting_infos[0] = _create_row(
                book_dish_ids,
                row=mounting_infos[2],
                element=element,
                infos_archive=infos_archive,
                samples_reference=mounting_infos[0]
                )
            mounting_infos[2] += 1
    if saving:
        book_dish_ids.save(path_dish_ids)
    return True

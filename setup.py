# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
Setup file of hcs_naming.
This file is used by pip to install this module.
It will automatically install needed packages.
"""

from setuptools import\
    setup,\
    find_packages

MAJOR = 0
MINOR = 1
PATCH = 0
VERSION = "{}.{}.{}".format(MAJOR, MINOR, PATCH)

CLASSIFIERS = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Science/Research",
    "Intended Audience :: Developers",
    "Intended Audience :: sys admins",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.6",
    "Topic :: Data handling",
    "Topic :: Scientific/Engineering",
    "Operating System :: Unix"
]

with open("hcs_naming/version.py", "w") as f:
    f.write("__version__ = '{}'\n".format(VERSION))

setup(
    name="hcs_naming",
    author="sylv.lempereur@gmail.com",
    description="Generate TPS name for Nikon Jobs acquisitions",
    version=VERSION,
    license="BSD",
    classifiers=CLASSIFIERS,
    packages=find_packages(),
    install_requires=["openpyxl"],
    scripts=[
        'calls/sampleid_attribution',
        'calls/nikon_jobs_rename'
        ],
)

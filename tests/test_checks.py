"""
File containing functions used to test
hcs_naming.sampleid_attribution._check_status_vs_sample_status.
"""

import pytest

from hcs_naming.sampleid_attribution._check_status_vs_sample_status\
    import _check_status_vs_sample_status

from .variable_checks import VarChecks

VAR_CHECKS = VarChecks()

@pytest.mark.parametrize('general', VAR_CHECKS.get_status())
@pytest.mark.parametrize('test', VAR_CHECKS.get_tests())
def test_checks(general, test):
    """
    Use case testing
    """
    expected = (
        test[general]["general"],
        test[general]["samples"]
        )
    results = _check_status_vs_sample_status(general, test["samples"])
    assert expected == results

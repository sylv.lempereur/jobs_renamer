"""
Contains variable used to test ability to read mounting_plate_status files.
"""

class VarMountingPlateStatus:
    """
    This class contains expected results by reading Mounting_plate_status.
    """
    def __init__(self):
        """
        Generate test examples.
        """
        self._path_mounting = "./tests/ref_files/"\
            + "test_mounting_plate_status.xlsx"
        self._path_mounting_unsaved = "./tests/ref_files/"\
            + "test_mounting_plate_status_unsaved.xlsx"
        self._path_get_mounting_plates_update_test = "./tests/ref_files/"\
            + "test_mounting_plate_status_update_test.xlsx"

        self._plates = {
            "sample ids":{
                "010101-A": [
                    "1a",
                    "1b",
                    "2a",
                    "2b",
                    ],
                "010101-B": [
                    "3a",
                    "4a",
                    None,
                    "5a",
                    "6a",
                    None,
                    ],
                "010102-C": [
                    "1c",
                    ],
                },
            "TPS names bases":{
                "010101-A": [
                    "1a_1dpf_AA-1-BB",
                    "1b_1dpf_AA-1-BB",
                    "2a_1dpf_AA-1-BB",
                    "2b_1dpf_AA-1-BB",
                    ],
                "010101-B": [
                    "3a_1dpf_AA-1-BB",
                    "4a_1dpf_AA-1-BB",
                    "None",
                    "5a_unknow_AA-2-CC",
                    "6a_1dpf_AA-2-CC",
                    "None",
                    ],
                "010102-C": [
                    "1c_1dpf_AA-1-BB",
                    ],
                },
            "Acquisition comments":{
                "010101-A": [
                    "a",
                    "b",
                    "c",
                    "d",
                    ],
                "010101-B": [
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    ],
                "010102-C": [
                    None,
                    ],
                },
            "Analysis comments":{
                "010101-A": [
                    None,
                    None,
                    None,
                    None,
                    ],
                "010101-B": [
                    "e",
                    "f",
                    None,
                    "h",
                    "g",
                    None,
                    ],
                "010102-C": [
                    None,
                    ],
                },
            "sample status":{
                "010101-A": [
                    0,
                    2,
                    1,
                    0,
                    ],
                "010101-B": [
                    1,
                    2,
                    -1,
                    2,
                    1,
                    -1
                    ],
                "010102-C": [
                    2,
                    ],
                }
            }

        self._mounting_infos = [
            {
                '1': 3,
                '2': 2,
                '3': 1,
                '4': 1,
                '5': 1,
                '6': 1
            },
            {
                "010101-A": {
                    'row': 2,
                    'samples': [1, 1, 2, 2]
                },
                "010101-B": {
                    'row': 3,
                    'samples': [3, 4, "None", 5, 6, "None"]
                },
                "010102-C": {
                    'row': 4,
                    'samples': [1]
                }
            },
            5
        ]

        self._headers = [
            "dish identifier",
            "Status"]
        for i in range(1, 102):
            self._headers.append('sample ' + str(i))

        self._ref_infos = {
            "010101-A": {
                "status": "Unknow",
                "sample_status":\
                    self._plates["sample status"]['010101-A'],
                "acquisition comments":\
                    self._plates["Acquisition comments"]['010101-A'],
                "analysis comments":\
                    self._plates["Analysis comments"]['010101-A'],
                },
            "010101-B": {
                "status": "Imaged",
                "sample_status":\
                    self._plates["sample status"]['010101-B'],
                "acquisition comments":\
                    self._plates["Acquisition comments"]['010101-B'],
                "analysis comments":\
                    self._plates["Analysis comments"]['010101-B'],
                },
            "010102-C": {
                "status": "Analysed",
                "sample_status":\
                    self._plates["sample status"]["010102-C"],
                "acquisition comments":\
                    self._plates["Acquisition comments"]['010102-C'],
                "analysis comments":\
                    self._plates["Analysis comments"]['010102-C'],
                }
            }


    def get_headers(self):
        """
        Return example headers
        """
        return self._headers


    def get_mounting_infos(self):
        """
        Return example mounting_infos
        """
        return self._mounting_infos

    def get_path_mounting(self):
        """
        Return path to example mounting_plate_status.
        """
        return self._path_mounting

    def get_path_mounting_unsaved(self):
        """
        Return path to example mounting_plate_status.
        """
        return self._path_mounting_unsaved

    def get_plates(self):
        """
        Return example plate dictionnary
        """
        return self._plates

    def get_ref_infos(self):
        """
        Return path to example mounting_plate_status.
        """
        return self._ref_infos

    def get_path_get_update_test(self):
        """
        Return path to get_mounting_path.xlsx used for tests.
        """
        return self._path_get_mounting_plates_update_test

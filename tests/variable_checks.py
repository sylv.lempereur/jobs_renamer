"""
File containing dictionnary used to test check functions.

General shape of a test variable:
$ case = {
$   "general": general status of a plate
$   "samples": list of sample status values
$   "Analysed": Excpected results of _check_analysed
$   "imaged": Excpected results of _check_imaged
$   "Unknow": Excpected results of _check_unknow
"""

class VarChecks:
    """
    This class contains variables to test checking functions.
    """
    def __init__(self):
        """
        Generation of test exxamples
        """
        # Every samples are analysed
        analysed = {
            "samples":[-3, -2, -1, 2, 2, 2],
            "Analysed": {
                "general": "Analysed",
                "samples": [-3, -2, -1, 2, 2, 2]
                },
            "Imaged": {
                "general": "Analysed",
                "samples": [-3, -2, -1, 2, 2, 2]
                },
            "Unknow": {
                "general": "Analysed",
                "samples": [-3, -2, -1, 2, 2, 2]
                }
            }

        # Mix of analysed and imaged
        analysed_and_imaged = {
            "samples":[-3, -2, -1, 1, 1, 2],
            "Analysed": {
                "general": "Analysed",
                "samples": [-3, -2, -1, -3, -3, 2]
                },
            "Imaged": {
                "general": "Imaged",
                "samples": [-3, -2, -1, 1, 1, 2]
                },
            "Unknow": {
                "general": "Imaged",
                "samples": [-3, -2, -1, 1, 1, 2]
                }
            }

        # Mix of analysed and unknow
        analysed_and_unknow = {
            "samples":[-3, -2, -1, 0, 2, 2],
            "Analysed": {
                "general": "Analysed",
                "samples": [-3, -2, -1, -4, 2, 2]
                },
            "Imaged": {
                "general": "Analysed",
                "samples": [-3, -2, -1, -2, 2, 2]
                },
            "Unknow": {
                "general": "Unknow",
                "samples": [-3, -2, -1, 0, 2, 2]
                }
            }

        # Mix of everything
        everything = {
            "samples":[-3, -2, -1, 0, 1, 2],
            "Analysed": {
                "general": "Analysed",
                "samples": [-3, -2, -1, -4, -3, 2]
                },
            "Imaged": {
                "general": "Imaged",
                "samples": [-3, -2, -1, -2, 1, 2]
                },
            "Unknow": {
                "general": "Unknow",
                "samples": [-3, -2, -1, 0, 1, 2]
                }
            }

        # Imaged
        imaged = {
            "samples":[-3, -2, -1, 1, 1, 2],
            "Analysed": {
                "general": "Analysed",
                "samples": [-3, -2, -1, -3, -3, 2]
                },
            "Imaged": {
                "general": "Imaged",
                "samples": [-3, -2, -1, 1, 1, 2]
                },
            "Unknow": {
                "general": "Imaged",
                "samples": [-3, -2, -1, 1, 1, 2]
                }
            }

        # Imaged
        imaged_and_unknow = {
            "samples":[-3, -2, -1, 0, 1, 1],
            "Analysed": {
                "general": "Analysed",
                "samples": [-3, -2, -1, -4, 2, 2]
                },
            "Imaged": {
                "general": "Imaged",
                "samples": [-3, -2, -1, -2, 1, 1]
                },
            "Unknow": {
                "general": "Unknow",
                "samples": [-3, -2, -1, 0, 1, 1]
                }
            }

        # Imaged
        unknow = {
            "samples":[-3, -2, -1, 0, 0, 0],
            "Analysed": {
                "general": "Analysed",
                "samples": [-3, -2, -1, 2, 2, 2]
                },
            "Imaged": {
                "general": "Imaged",
                "samples": [-3, -2, -1, 1, 1, 1]
                },
            "Unknow": {
                "general": "Unknow",
                "samples": [-3, -2, -1, 0, 0, 0]
                }
            }

        self._tests = [
            analysed,
            analysed_and_imaged,
            analysed_and_unknow,
            everything,
            imaged,
            imaged_and_unknow,
            unknow,
            ]

        self._status = ["Analysed", "Imaged", "Unknow"]

    def get_status(self):
        """
        Return status list
        """
        return self._status

    def get_tests(self):
        """
        Return status list
        """
        return self._tests

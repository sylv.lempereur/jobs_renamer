"""
Test hcs_naming.tools.get_headers
"""

from os import makedirs
from os.path import exists
from shutil import copyfile, rmtree

import openpyxl

import pytest

from hcs_naming.nikon_jobs_rename import\
    _get_acquisition_folders,\
    _get_jobs_infos,\
    _get_plates,\
    _get_tps_names,\
    create_tps_names,\
    rename_nikon_jobs_acquisitions

from hcs_naming.sampleid_attribution import\
    _get_general_infos,\
    _get_infos_from_mounting_status,\
    _get_ref_infos,\
    _get_modification_path,\
    _loads_infos,\
    create_sample_id_xlsx,\
    update_mounting_plates_status

from hcs_naming.tools import\
    check_status,\
    create_destination_folder,\
    get_headers,\
    get_sample_status_sheets,\
    set_formating

from .variable_archive import VarArchive

from .variable_mounting_plate_status import VarMountingPlateStatus as VarMouting

from .variable_jobs_correspondance import VarJobs

from .variable_tps_names import VarTPSNames

from .variable_get_mounting_plates import VarGetMountingPlates as VarGet

def test_get_general_infos():
    """
    Test _get_cgeneral_infos
    """
    var_archive = VarArchive()
    assert var_archive.get_archive()\
        == _get_general_infos(var_archive.get_path_archive())

def test_check_status_saved():
    """
    Test _check_status.
    """
    var_mouting = VarMouting()
    assert check_status(var_mouting.get_path_mounting(), saving=True)

def test_check_status_unsaved():
    """
    Test _check_status.
    """
    var_mouting = VarMouting()
    assert check_status(var_mouting.get_path_mounting_unsaved(), saving=False)

def test_get_headers():
    """
    Test ability to read header of a file.
    """
    var_mouting = VarMouting()
    book = openpyxl.load_workbook(var_mouting.get_path_mounting())
    headers = get_headers(book["sample ids"])
    assert headers == var_mouting.get_headers()

def test_get_plates():
    """
    Test ability to get TPS name bases from mounting_plate_status.
    """
    var_mouting = VarMouting()
    assert var_mouting.get_plates()["TPS names bases"] ==\
        _get_plates(var_mouting.get_path_mounting())

def test_get_ref_infos():
    """
    Test ability to get reference infos.
    """
    var_mouting = VarMouting()
    assert var_mouting.get_ref_infos()\
        == _get_ref_infos(var_mouting.get_path_mounting())

def test_get_sample_status_sheets():
    """
    Test get_sample_status_sheet function.
    """
    var_get = VarMouting()
    sheets = get_sample_status_sheets(
        openpyxl.load_workbook(var_get.get_path_mounting())
        )
    assert\
        (
            5,
            openpyxl.worksheet.worksheet.Worksheet,
            openpyxl.worksheet.worksheet.Worksheet,
            openpyxl.worksheet.worksheet.Worksheet,
            openpyxl.worksheet.worksheet.Worksheet,
            openpyxl.worksheet.worksheet.Worksheet
        ) ==\
        (
            len(sheets),
            type(sheets[0]),
            type(sheets[1]),
            type(sheets[2]),
            type(sheets[3]),
            type(sheets[4])
        )

def test_loads_infos_elements():
    """
    Test ability to get elements using _loads_infos
    """
    var_get = VarGet()
    infos = _loads_infos(var_get.get_path_get_mounting_plates())
    assert infos == (var_get.get_headers(), var_get.get_elements())

def test_get_jobs_infos():
    """
    Test get_jobs_infos.
    """
    var_jobs = VarJobs()
    assert _get_jobs_infos(var_jobs.get_path_jobs())\
        == var_jobs.get_infos()

def test_set_formating():
    """
    test set_formating
    """
    var_jobs = VarJobs()
    assert set_formating(var_jobs.get_path_mounting())

def test_get_infos_mounting_status():
    """
    Test get_infos_from_mounting_status function.
    """
    var_mounting = VarMouting()
    sheet = openpyxl.load_workbook(
        var_mounting.get_path_mounting()
        )["sample ids"]
    assert var_mounting.get_mounting_infos()\
        == _get_infos_from_mounting_status(sheet)

def test_update_mounting_status():
    """
    Test get_infos_from_mounting_status function.
    """
    var_mounting = VarMouting()
    var_get = VarGet()
    var_archive = VarArchive()
    update_mounting_plates_status(
        var_mounting.get_path_mounting(),
        var_get.get_path_get_plates_add_row(),
        var_archive.get_path_archive(),
        False)
    path_cp = "./tests/temp_mounting.xlsx"
    copyfile(
        var_mounting.get_path_get_update_test(),
        path_cp
        )
    update_mounting_plates_status(
        path_cp,
        var_get.get_path_get_mounting_plates(),
        var_archive.get_path_archive(),
        True)

def test_create_tps_name():
    """
    Test create_tps_name function.
    """
    var_jobs = VarJobs()
    var_mounting = VarMouting()
    var_tps = VarTPSNames()
    create_tps_names(
        var_jobs.get_path_jobs(),
        var_mounting.get_path_mounting(),
        var_tps.get_path_output(),
        saving=True
        )
    create_tps_names(
        var_jobs.get_path_jobs(),
        var_mounting.get_path_mounting(),
        var_tps.get_path_output(),
        saving=False
        )

def test_create_sample():
    """
    Test create_sample_id_xlsx function.
    """
    var_get = VarGet()
    var_archive = VarArchive()
    get_path_mounting_test = './tests/ref_files/creation_status.xlsx'
    create_sample_id_xlsx(
        var_get.get_path_get_mounting_plates(),
        get_path_mounting_test,
        var_archive.get_path_archive()
        )

def test_get_tps_names():
    """
    Test _get_tps_names
    """
    var_tps = VarTPSNames()
    print(var_tps.get_acquisitions())
    print(_get_tps_names(var_tps.get_path_tps_name()))
    assert var_tps.get_acquisitions()\
        == _get_tps_names(var_tps.get_path_tps_name())

def test_rename():
    """
    Test rename_nikon_jobs_acquisitions
    """
    folder_input = "./tests/ref_acquisitions/"
    folder_output = "./tests/test_acquisitions/"
    # makedirs(folder_output)
    var_tps = VarTPSNames()
    var_jobs = VarJobs()
    rename_nikon_jobs_acquisitions(
        folder_input,
        var_tps.get_path_tps_name(),
        var_jobs.get_path_jobs(),
        folder_output
        )
    if not exists(folder_output+'test_if_folder_exists/'):
        makedirs(folder_output+'test_if_folder_exists/')
        open(folder_output + 'test_if_folder_exists/debug.log', "w+")
    var_tps = VarTPSNames()
    var_jobs = VarJobs()
    rename_nikon_jobs_acquisitions(
        folder_input,
        var_tps.get_path_tps_name(),
        var_jobs.get_path_jobs(),
        folder_output+'test_if_folder_exists'
        )
    rmtree(folder_output)

def test_create_destination_folder():
    """
    Tests hcs_naming.tools.create_destionation_folder
    """
    create_destination_folder('./tests/')
    create_destination_folder('./tests/test_creation_folder/')
    rmtree('./tests/test_creation_folder/')

def test_get_acquisition_folders():
    """
    Tests hcs_naming.nikon_jobs_rename._get_acquisition_folders
    """
    folders = _get_acquisition_folders('./tests/ref_acquisitions')
    results = sorted(
        [
            "./tests/ref_acquisitions/20010101_000001_000",
            "./tests/ref_acquisitions/20010103_000001_000",
            "./tests/ref_acquisitions/20010104_000001_000",
            "./tests/ref_acquisitions/20010105_000001_000",
            "./tests/ref_acquisitions/20010106_000001_000",
            "./tests/ref_acquisitions/20010107_000001_000",
            "./tests/ref_acquisitions/20010108_000001_000",
            "./tests/ref_acquisitions/20010109_000001_000",
            "./tests/ref_acquisitions/test_recursivity/20010102_000001_000",
            ]
        )
    assert results == folders


@pytest.mark.parametrize(
    'tests',
    [
        (
            './tests/mounting_plate_status.xlsx',
            './tests/modifications.log'
            ),
        (
            'mounting_plate_status.xlsx',
            'modifications.log'
            )
        ]
    )
def test_get_modification_path(tests):
    path_with_basename = _get_modification_path(tests[0])
    assert path_with_basename == tests[1]

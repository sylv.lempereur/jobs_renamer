"""
Module to test hcs_naming module
"""

from .test_checks import (
    test_checks
    )

from .test_reads import (
    test_get_general_infos,
    test_check_status_saved,
    test_check_status_unsaved,
    test_get_headers,
    test_get_plates,
    test_get_ref_infos,
    test_get_sample_status_sheets,
    test_loads_infos_elements,
    test_get_jobs_infos,
    test_set_formating,
    test_get_infos_mounting_status,
    test_update_mounting_status,
    test_create_tps_name,
    test_create_sample,
    test_get_tps_names,
    test_rename,
    test_create_destination_folder,
    test_get_acquisition_folders,
    test_get_modification_path
    )

from .variable_archive import VarArchive
from .variable_checks import VarChecks
from .variable_get_mounting_plates import VarGetMountingPlates
from .variable_jobs_correspondance import VarJobs
from .variable_mounting_plate_status import VarMountingPlateStatus
from .variable_tps_names import VarTPSNames

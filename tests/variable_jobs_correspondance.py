"""
Contains variable used to test ability to read get_mounting_plate files.
"""

class VarJobs:
    """
    This class contains expected results by reading getMoutingPlates.
    """
    def __init__(self):
        """
        Generate example to test get_mounting_plate.xlsx
        """
        # Path to the test files.
        self._path_jobs_correspondance = "./tests/ref_files/"\
            + "jobs_correspondance_test.xlsx"
        self._path_mounting = "./tests/ref_files/"\
            + "test_mounting_plate_status_jobs.xlsx"

        self._infos = {
            "20010101_000001_000":{
                "plate": "010101-A",
                "status": "merge",
                "order": -1,
                "acquirer": "Aa",
                "resolution": "512",
                "comment": "a"
            },
            "20010101_000002_000":{
                "plate": "010101-A",
                "status": "tiles",
                "order": 1,
                "acquirer": "Ab",
                "resolution": "256",
                "comment": None
            },
            "20010102_000001_000":{
                "plate": "010101-B",
                "status": "merge",
                "order": 1,
                "acquirer": "Ac",
                "resolution": "1024",
                "comment": "s"
            },
            "20010103_000001_000":{
                "plate": "010102-C",
                "status": "tiles",
                "order": -1,
                "acquirer": "Ba",
                "resolution": "512",
                "comment": None
            },
            "20010104_000001_000":{
                "plate": "010102-C",
                "status": "merge",
                "order": 1,
                "acquirer": "Bb",
                "resolution": "256",
                "comment": "test"
            },
            "20010105_000001_000":{
                "plate": "010102-D",
                "status": "merge",
                "order": 1,
                "acquirer": "Bb",
                "resolution": "256",
                "comment": "test"
            },
        }

    def get_infos(self):
        """
        Return infos in get_mounting_path.xlsx.
        """
        return self._infos

    def get_path_jobs(self):
        """
        Return path to get_mounting_path.xlsx used for tests.
        """
        return self._path_jobs_correspondance

    def get_path_mounting(self):
        """
        Return path to get_mounting_path.xlsx used for tests.
        """
        return self._path_mounting

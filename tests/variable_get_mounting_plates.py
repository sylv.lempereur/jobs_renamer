"""
Contains variable used to test ability to read get_mounting_plate files.
"""

class VarGetMountingPlates:
    """
    This class contains expected results by reading getMoutingPlates.
    """
    def __init__(self):
        """
        Generate example to test get_mounting_plate.xlsx
        """
        # Path to the test file.
        self._path_get_mounting_plates = "./tests/ref_files/"\
            + "test_get_mounting_plates.xlsx"
        self._path_get_mounting_plates_add_row = "./tests/ref_files/"\
            + "test_get_mounting_plates_add_row.xlsx"

        self._elements = [
            [
                "010101-A", 1, 1, 2, 2
            ],
            [
                "010101-B", 3, 4, "None", 5, 6, "None"
            ],
            [
                "010102-C", 1
            ],
        ]

        self._headers = ['dish identifier']

        for i in range(1, 102):
            self._headers.append("sample "+str(i))

    def get_elements(self):
        """
        Return elements in get_mounting_path.xlsx.
        """
        return self._elements

    def get_headers(self):
        """
        Return headers of get_mounting_path.xlsx.
        """
        return self._headers

    def get_path_get_mounting_plates(self):
        """
        Return path to get_mounting_path.xlsx used for tests.
        """
        return self._path_get_mounting_plates

    def get_path_get_plates_add_row(self):
        """
        Return path to get_mounting_path.xlsx used for tests.
        """
        return self._path_get_mounting_plates_add_row

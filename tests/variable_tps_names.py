"""
Contains variable used to test ability to read get_mounting_plate files.
"""

class VarTPSNames:
    """
    This class contains expected results by reading getMoutingPlates.
    """
    def __init__(self):
        """
        Generate example to test get_mounting_plate.xlsx
        """
        # Path to the test file.
        self._path_tps_name = "./tests/ref_files/"\
            + "tps_name_test.xlsx"
        self._path_output = "./tests/ref_files/"\
            + "tps_name_output.xlsx"

        self._headers = ['dish identifier']

        for i in range(1, 102):
            self._headers.append("sample "+str(i))

        self._acquisitions = {
            "20010101_000001_000": {
                "samples": [
                    "010101Aa_2b_1dpf_AA-1-BB_512_Nh_merge.txt",
                    "010101Aa_2a_1dpf_AA-1-BB_512_Nh_merge.txt",
                    "010101Aa_1b_1dpf_AA-1-BB_512_Nh_merge.txt",
                    "010101Aa_1a_1dpf_AA-1-BB_512_Nh_merge.txt",
                ],
                "plate": "010101-A"
                },
            "20010101_000002_000": {
                "samples": [
                    "010101Ab_1a_1dpf_AA-1-BB_512_Nh_merge.txt",
                    "010101Ab_1b_1dpf_AA-1-BB_512_Nh_merge.txt",
                    "010101Ab_2a_1dpf_AA-1-BB_512_Nh_merge.txt",
                    "010101Ab_2b_1dpf_AA-1-BB_512_Nh_merge.txt",
                ],
                "plate": "010101-A"
                },
            "20010102_000001_000": {
                "samples": [
                    "010102Ab_3a_1dpf_AA-1-BB_1024_Nh_tiles.txt",
                    "010102Ab_4a_1dpf_AA-1-BB_1024_Nh_tiles.txt",
                    "None",
                    "010102Ab_5a_1dpf_AA-2-CC_1024_Nh_tiles.txt",
                    "010102Ab_6a_1dpf_AA-2-CC_1024_Nh_tiles.txt",
                    "None",
                    ],
                "plate": "010101-B"
                },
            "20010103_000001_000": {
                "samples": [
                    "010103Ba_1c_1dpf_AA-1-BB_512_Nh_merge.txt",
                    ],
                "plate": "010102-C"
                },
            "20010104_000001_000": {
                "samples": [
                    "010104Bb_1c_1dpf_AA-1-BB_512_Nh_merge.txt",
                    ],
                "plate": "010102-C"
                },
        }

        self._renamed = {
            '010101-A': {
                "samples": ["1a", "1b", "2a", "2b"],
                "comment": ""
            },
            '010101-B': {
                "samples": ["3a", "4a", "5a", "6a"],
                "comment": ""
            },
            '010102-C': {
                "samples": ["1c"],
                "comment": ""
            }
        }

    def get_acquisitions(self):
        """
        Return _acquisition used for test
        """
        return self._acquisitions

    def get_path_tps_name(self):
        """
        Return path to get_mounting_path.xlsx used for tests.
        """
        return self._path_tps_name

    def get_path_output(self):
        """
        Return path to get_mounting_path.xlsx used for tests.
        """
        return self._path_output

    def get_renamed(self):
        """
        Return _renamed used for test
        """
        return self._renamed

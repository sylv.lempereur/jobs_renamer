"""
Contains variable used to test ability to read mounting_plate_status files.
"""

class VarArchive:
    """
    This class contains expected results by reading Mounting_plate_status.
    """
    def __init__(self):
        """
        Generate test examples.
        """
        self._path_archive = "./tests/ref_files/"\
            + "test_archive.xlsx"

        self._archive = {
            "1": [
                "AA-1-BB",
                "1dpf"
                ],
            "2": [
                "None",
                "1dpf"
                ],
            "4": [
                "AA-1-BB",
                "1dpf"
                ],
            "3": [
                "AA-1-BB",
                "1dpf"
                ],
            "5": [
                "AA-2-CC",
                "unknow"
                ],
            "6": [
                "AA-2-CC",
                "1dpf"
                ],
            "7": [
                "AA-2-CC",
                "unknow"
                ],
            "8": [
                "AA-1-BB",
                "1dpf"
                ],
            "10": [
                "AA-1-BB",
                "1dpf"
                ]
            }

    def get_path_archive(self):
        """
        Return path to example archive.xlsx.
        """
        return self._path_archive

    def get_archive(self):
        """
        Return archive test values
        """
        return self._archive
